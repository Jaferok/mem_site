from django.contrib.auth import views as auth_views
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path


urlpatterns = [
    path(r'admin/', admin.site.urls),
    url(r'^accounts/password_reset/$', auth_views.PasswordResetView.as_view(
        template_name='registration/password_reset_new.html')),
    url(r'^accounts/password_reset/done/$', auth_views.PasswordResetDoneView.as_view(
        template_name='registration/password_reset_done_new.html')),
    url(r'^accounts/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,24})/$',
        auth_views.PasswordResetConfirmView.as_view(template_name='registration/password_reset_confirm_new.html')),
    url(r'^accounts/reset/done/$', auth_views.PasswordResetCompleteView.as_view(
        template_name='registration/password_reset_complete_new.html')),
    url(r'accounts/', include("django.contrib.auth.urls")),
    url(r'', include("memes.urls")),
]
