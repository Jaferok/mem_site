from django.conf.urls import url, include
from . import views
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', views.mem_list, name="mem_list"),
    url(r'^memchik/(?P<pk>\d+)/$', views.mem_detail, name="mem_detail"),
    url(r'^mem/new/$', views.mem_new, name="mem_new"),
    url(r'^mem/(?P<pk>\d+)/edit/$', views.mem_edit, name='mem_edit'),
    url(r'^drafts/$', views.mem_draft_list, name="mem_draft_list"),
    url(r'^mem/(?P<pk>\d+)/publish/$', views.mem_publish, name="mem_publish"),
    url(r'^mem/(?P<pk>\d+)/remove/$', views.mem_remove, name="mem_remove"),
    url(r'^mem/(?P<pk>\d+)/comment/$', views.add_comment_to_mem, name="add_comment_to_mem"),
    url(r'^comment/(?P<pk>\d+)/approve/$', views.comment_approve, name="comment_approve"),
    url(r'^comment/(?P<pk>\d+)/remove/$', views.comment_remove, name="comment_remove"),
    url(r'^register/$', views.register, name="register"),
    url(r'^ratings/', include('star_ratings.urls', namespace='ratings')),
    url(r'^memology/$', views.memology_list, name="memology_list"),
]


if settings.DEBUG:

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
