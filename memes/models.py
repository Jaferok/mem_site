from django.db import models
from django.utils import timezone
from django.utils.safestring import mark_safe


class Mem(models.Model):

    author = models.ForeignKey("auth.User", on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    mem_history = models.TextField(blank=True, null=True)
    mem_image = models.ImageField(upload_to='images/%Y/%m/%d')
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)


    def publish(self):

        self.published_date = timezone.now()
        self.save()


    def __str__(self):

        return self.title


    def image(self):

        if self.image:

            return mark_safe(u'<a href="{0}" target="_blank"><img src="{0}"/></a>'.format(self.mem_image.url))

        else:

            return "No image"

    image.short_description = "Image"
    image.allow_tags = True


    def approved_comments(self):

        return self.comments.filter(approved_comment=True)


class Comment(models.Model):

    mem = models.ForeignKey("memes.Mem", on_delete=models.CASCADE, related_name="comments")
    author = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved_comment = models.BooleanField(default=False)

    def approve(self):

        self.approved_comment = True
        self.save()


    def __str__(self):

        return self.text

