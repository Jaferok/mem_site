from django import forms
from .models import Mem, Comment


class MemForm(forms.ModelForm):

    class Meta:

        model = Mem
        fields = ("title", "mem_image", "mem_history",)


class CommentForm(forms.ModelForm):

    class Meta:

        model = Comment
        fields = ("author", "text",)

# class MemologyForm(forms.ModelForm):

#     class Meta:

#         model = Mem_for_Memology
#         fields = ("title", "mem_image", "mem_history", )