from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.forms import UserCreationForm
from django.contrib import auth
from django.utils import timezone
from .models import Mem, Comment
from .forms import MemForm, CommentForm
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator


def mem_list(request, *page_size):

    page_size = 5
    memesys = Mem.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    memesys = Mem.objects.filter(mem_history__regex=r'^$')
    paginator = Paginator(memesys, page_size)

    page = request.GET.get("page")
    memesy = paginator.get_page(page)
    return render(request, 'memes/mem_list.html', {"memesy": memesy})


def memology_list(request):

    page_size = 5
    memesys = Mem.objects.filter(published_date__lte=timezone.now()).order_by("published_date")
    memesys = Mem.objects.filter(mem_history__regex=r'[0-9A-Za-z_\-]+')
    paginator = Paginator(memesys, page_size)

    page = request.GET.get("page")
    memesy = paginator.get_page(page)
    return render(request, "memes/memology_list.html", {"memesy": memesy})


@login_required
def mem_new(request):

    if request.method == "POST":

        form = MemForm(request.POST, request.FILES)

        if form.is_valid():

            memchik = form.save(commit=False)

            if memchik.mem_history:

                memchik.author = request.user
                memchik.save()
                return redirect("mem_detail", pk=memchik.pk)

            else:

                memchik.author = request.user
                memchik.save()
                return redirect("mem_detail", pk=memchik.pk)
    else:

        form = MemForm()

    return render(request, "memes/mem_edit.html", {"form": form})


# @login_required
# def new_mem_for_memology(request):

#     if request.method == "POST":

#         form = MemologyForm(request.POST, request.FILES)

#         if form.is_valid():

#             memasik = form.save(commit=False)
#             memasik.author = request.user
#             memasik.save()
#             return redirect("memology_detail", pk=memasik.pk)
#     else:

#         form = MemologyForm()

#     return render(request, "memes/memology_detail.html", {"form": form})


# def memology_detail(request, pk):

#     mem = get_object_or_404(Mem_for_Memology, pk=pk)
#     return render(request, "memes/memology_edit.html", {"mem": mem})


def mem_detail(request, pk):

    mem = get_object_or_404(Mem, pk=pk)
    return render(request, "memes/mem_detail.html", {"mem": mem})


@login_required
def mem_edit(request, pk):

    mem = get_object_or_404(Mem, pk=pk)
    if request.method == "POST":

        form = MemForm(request.POST, request.FILES, instance=mem)

        if form.is_valid():

            mem = form.save(commit=False)
            mem.author = request.user
            mem.save()
            return redirect("mem_detail", pk=mem.pk)

    else:

        form = MemForm(instance=mem)

    return render(request, "memes/mem_edit.html", {"form": form})


@login_required
def mem_draft_list(request):

    memeses = Mem.objects.filter(published_date__isnull=True).order_by("created_date")
    return render(request, "memes/mem_draft_list.html", {"memeses": memeses})


@login_required
def mem_publish(request, pk):

    mem = get_object_or_404(Mem, pk=pk)
    mem.publish()
    return redirect("mem_detail", pk=pk)


@login_required
def mem_remove(request, pk):

    mem = get_object_or_404(Mem, pk=pk)
    mem.delete()
    return redirect("mem_list")


def add_comment_to_mem(request, pk):

    mem = get_object_or_404(Mem, pk=pk)
    if request.method == "POST":

        form = CommentForm(request.POST)

        if form.is_valid():

            comment = form.save(commit=False)
            comment.mem = mem
            comment.save()
            return redirect("mem_detail", pk=mem.pk)

    else:

        form = CommentForm()

    return render(request, "memes/add_comment_to_mem.html", {"form": form})


@login_required
def comment_approve(request, pk):

    comment = get_object_or_404(Comment, pk=pk)
    comment.approve()
    return redirect("mem_detail", pk=comment.mem.pk)


@login_required
def comment_remove(request, pk):

    comment = get_object_or_404(Comment, pk=pk)
    comment.delete()
    return redirect("mem_detail", pk=comment.mem.pk)


def register(request):

    context = {}
    register_form = UserCreationForm()
    context = {"register_form": register_form}

    if request.method == "POST":

        new_user_form = UserCreationForm(request.POST)

        if new_user_form.is_valid():

            new_user_form.save()
            new_user = auth.authenticate(username=new_user_form.cleaned_data["username"],
                                         password=new_user_form.cleaned_data["password2"])
            auth.login(request, new_user)
            return redirect("/")

        else:

            register_form = new_user_form
            context = {"register_form": register_form}

    return render(request, "registration/register.html", context)