from django.contrib import admin
from .models import Mem, Comment

# Register your models here.

admin.site.register(Mem)
admin.site.register(Comment)
